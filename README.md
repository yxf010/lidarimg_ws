# lidarimg_ws

## 介绍
激光雷达与相机融合车辆检测

## 环境
Ubuntu16.04、ros-melodic、opencv3.4、pcl1.7

## 编译
git clone https://gitee.com/leox24/lidarimg_ws.git  
catkin_make  
激光雷达检测：roslaunch obstacle_detection tradition.launch  
地面分割：roslaunch ground_segmentation ground_seg.launch  
图像激光雷达后融合：roslaunch fusionv2 fusionv2.launch  

## 功能模块
### fusion
利用opencv库自带的yolo网络进行图像障碍物检测，载入相应的模型和权重即可。opencv库仅利用cpu，速度较慢，后续更改为fusionV2。 
激光雷达和图像的数据利用KITTI的raw data，转换成bag包，利用ros的时间同步回调功能实现时间同步，空间同步利用外内参矩阵相乘即可。
 
### fusionv2
利用yolov3官网的代码编译好后，链接动态库，复制darknet.h等头文件，采用GPU和cuda加速实现图像检测，速度较快。  
激光雷达的障碍物聚类的bbox信息和图像的车辆box信息同步融合，最近领中心点匹配，匹配结果更新为最终车辆检测。  
**![fusion](https://images.gitee.com/uploads/images/2020/0711/141839_3ad6a9a9_2197075.png "屏幕截图.png")** 


### ground_segmentation
地面分割。精简[Run_based_segmentation](https://github.com/VincentCheungM/Run_based_segmentation.git)的代码，将地面划分为多个网格，处理第二平面如台阶，人行道，以及上下斜坡的问题，每个网格进行ground plane fitting主程序处理，实现地面分割。 
![分割](https://images.gitee.com/uploads/images/2020/0711/142110_92902094_2197075.png "图片3.png")

### imagedetec
利用opencv库自带的yolo网络进行图像障碍物检测demo

### kitti_pub
KITTI数据集的激光雷达bin文件发布成ros的消息格式sensor_msgs::PointCloud2，voxelnet里面截取出来的Python文件

### lidarmarkers_msgs
将激光雷达的障碍物的bbox的markers数据加上一个header数据，存储时间戳，使融合时间同步。

### obstacle_detection
激光雷达障碍物检测，利用环形欧式聚类，不同环形距离下采用不同的聚类阈值，使用PCL库实现。使用霍夫直线检测计算障碍物的主方向，进而计算障碍物的方向包围框。   
![cluster1](https://images.gitee.com/uploads/images/2020/0711/142139_5630fd2d_2197075.png "图片2.png")
![cluster2](https://images.gitee.com/uploads/images/2020/0711/142208_1300c2ca_2197075.png "图片1.png")
![cluster](https://images.gitee.com/uploads/images/2020/0711/140846_fe923bbf_2197075.png "2019-12-22 20-14-44 的屏幕截图.png")

### ttc
TODO

### yolo
存储yolo训练权重和网络模型

