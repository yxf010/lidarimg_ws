#include "detection_core.h"


int main(int argc, char **argv)
{
    ros::init(argc, argv, "obstacle_detection");
    ros::NodeHandle nh("~");
    DetectionCore core(nh);
}
