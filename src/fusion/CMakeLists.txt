cmake_minimum_required(VERSION 2.8.3)
project(fusion)
add_definitions(-std=c++11)
set(CXX_FLAGS "-Wall")
set(CMAKE_CXX_FLAGS, "${CXX_FLAGS}")
# add_compile_options(-std=c++11)
# SET(CMAKE_CXX_FLAGS "-std=c++11 -O2 -g -Wall ${CMAKE_CXX_FLAGS}")

find_package(catkin REQUIRED COMPONENTS
  cv_bridge
  image_transport
  message_filters
  roscpp
  rospy
  std_msgs
  tf
  tf_conversions
  sensor_msgs
)

find_package(PCL 1.7 REQUIRED)
find_package(OpenCV 3.4 REQUIRED)

catkin_package(
)


include_directories(
  ${catkin_INCLUDE_DIRS}
  /usr/include/boost-1.60.0/include
  /usr/local/include/opencv2
  /usr/local/include/opencv
  /usr/include/pcl-1.7
)

include_directories(${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

include_directories(${OpenCV_INCLUDE_DIRS})
link_directories(${OpenCV_LIBRARY_DIRS})
add_definitions(${OpenCV_DEFINITIONS})

add_executable(${PROJECT_NAME}_node src/fusion.cc )
target_link_libraries(${PROJECT_NAME}_node ${PCL_LIBRARIES} ${catkin_LIBRARIES} ${OpenCV_LIBS})